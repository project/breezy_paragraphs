<?php

namespace Drupal\breezy_paragraphs_ui\Form;

use Drupal\breakpoint\BreakpointManagerInterface;
use Drupal\breezy_paragraphs\Service\BreezyParagraphsBehaviorVariantPluginManagerInterface;
use Drupal\breezy_utility\Form\BreezyUtilityEntityAjaxFormTrait;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityWithPluginCollectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for editing BreezyParagraphsVariant entities.
 */
class BreezyParagraphsVariantEditForm extends EntityForm implements ContainerInjectionInterface {

  use BreezyUtilityEntityAjaxFormTrait;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Drupal\breakpoint\BreakpointManagerInterface definition.
   *
   * @var \Drupal\breakpoint\BreakpointManagerInterface
   */
  protected BreakpointManagerInterface $breakpointManager;

  /**
   * BreezyParagraphsBehaviorVariantPluginManagerInterface definition.
   *
   * @var \Drupal\breezy_paragraphs\Service\BreezyParagraphsBehaviorVariantPluginManagerInterface
   */
  protected BreezyParagraphsBehaviorVariantPluginManagerInterface $variantPluginManager;

  /**
   * Constructs a new BreezyParagraphsVariantForm form object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\breezy_paragraphs\Service\BreezyParagraphsBehaviorVariantPluginManagerInterface $variant_plugin_manager
   *   The variant plugin manager.
   * @param \Drupal\breakpoint\BreakpointManagerInterface $breakpoint_manager
   *   The breakpoint manager service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, BreezyParagraphsBehaviorVariantPluginManagerInterface $variant_plugin_manager, BreakpointManagerInterface $breakpoint_manager) {
    $this->configFactory = $config_factory;
    $this->variantPluginManager = $variant_plugin_manager;
    $this->breakpointManager = $breakpoint_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /** @var \Drupal\breakpoint\BreakpointManagerInterface $breakpoint_manager */
    $breakpoint_manager = $container->get('breakpoint.manager');
    /** @var \Drupal\Core\Config\ConfigFactoryInterface $config_factory */
    $config_factory = $container->get('config.factory');
    /** @var \Drupal\breezy_paragraphs\Service\BreezyParagraphsBehaviorVariantPluginManagerInterface $variant_plugin_manager */
    $variant_plugin_manager = $container->get('plugin.manager.breezy_paragraphs.behavior_variant');
    return new static($config_factory, $variant_plugin_manager, $breakpoint_manager);
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $input = $form_state->getUserInput();
    $plugin_form_wrapper = 'plugin-form-wrapper';

    /** @var \Drupal\breezy_paragraphs\Entity\BreezyParagraphsVariantInterface $variant */
    $variant = $this->entity;
    $form_state->set('variant', $variant);

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $variant->label(),
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $variant->id(),
      '#machine_name' => [
        'exists' => '\Drupal\breezy_paragraphs\Entity\BreezyParagraphsBehaviorVariant::load',
      ],
      '#disabled' => !$variant->isNew(),
    ];

    $plugin_id = $variant->getPluginId();
    $plugin_configuration = $input['plugin_configuration'] ?? $variant->getPluginConfiguration();

    if (!$plugin_id) {
      $form['plugin_id'] = [
        '#type' => 'select',
        '#title' => $this->t('Choose a paragraph type'),
        '#required' => TRUE,
        '#empty_option' => $this->t('- Select -'),
        '#options' => $this->getVariantPluginOptions(),
        '#default_value' => $form_state->getValue('plugin_id') ?? '',
        '#ajax' => [
          'callback' => '::pluginIdCallback',
          'wrapper' => $plugin_form_wrapper,
          'event' => 'change',
        ],
      ];
    }
    else {
      $form_state->set('plugin_id', $plugin_id);
      $form_state->setValue('plugin_id', $plugin_id);
      $form['plugin_id_display'] = [
        '#type' => 'item',
        '#title' => $this->t('@paragraph_type', ['@paragraph_type' => $plugin_id]),
      ];
      $form['plugin_id'] = [
        '#type' => 'hidden',
        '#value' => $plugin_id,
      ];

    }

    if ($form_state->getValue('plugin_id')) {
      $plugin_id = $form_state->getValue('plugin_id');
      $form_state->set('plugin_id', $plugin_id);
    }

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $variant->get('status') ?? '',
    ];

    if (!empty($plugin_id)) {
      if (!$plugin_configuration) {
        $plugin_configuration = [];
      }
      if (!isset($plugin_configuration['_entity'])) {
        $plugin_configuration['_entity'] = $variant->id();
      }
      /** @var \Drupal\breezy_paragraphs\Plugin\BreezyParagraphs\BehaviorVariant\BehaviorVariantInterface $plugin */
      $plugin = $this->variantPluginManager->createInstance($plugin_id, $plugin_configuration);
      $form['paragraph_type'] = [
        '#type' => 'hidden',
        '#value' => $plugin->getParagraphType(),
      ];
      $plugin_form = [
        '#type' => 'container',
        '#attributes' => [
          'id' => $plugin_form_wrapper,
        ],
        '#tree' => TRUE,
      ];
      $form['plugin_configuration'] = $plugin->buildConfigurationForm($plugin_form, $form_state);
    }

    return $this->buildAjaxForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function actionsElement(array $form, FormStateInterface $form_state) {
    $form = parent::actionsElement($form, $form_state);
    $form['submit']['#value'] = $this->t('Save elements');
    unset($form['delete']);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\breezy_paragraphs\Entity\BreezyParagraphsVariantInterface $variant */
    $variant = $this->entity;
    $status = $variant->save();

    $this->messenger()->addStatus($this->t('Saved'));
    return $status;
  }

  /**
   * {@inheritdoc}
   */
  protected function copyFormValuesToEntity(EntityInterface $entity, array $form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    if ($this->entity instanceof EntityWithPluginCollectionInterface) {
      // Do not manually update values represented by plugin collections.
      $values = array_diff_key($values, $this->entity->getPluginCollections());
    }

    $entity
      ->set('label', $values['label'])
      ->set('status', $values['status'])
      ->set('plugin_id', $values['plugin_id']);

    if ($plugin_id = $values['plugin_id']) {
      $paragraph_type = $this->variantPluginManager->getParagraphType($plugin_id);
      if ($paragraph_type) {
        $entity->set('paragraph_type', $paragraph_type);
      }
    }

    if (isset($values['plugin_configuration']) && !empty($values['plugin_configuration']) && !empty($values['plugin_id'])) {
      $plugin_configuration = $entity->getPluginConfiguration();
      // @todo Merge $form_state with $plugin_configuration.
      // @see BehaviorVariantBase::mergeFormState.
      $config_values = [];
      if (isset($plugin_configuration['breakpoints']) && $breakpoints = $plugin_configuration['breakpoints']) {
        foreach ($breakpoints as $breakpoint_name => $breakpoint_settings) {
          // If the entity settings are not set.
          if (!isset($breakpoint_settings['enabled']) || !$breakpoint_settings['enabled']) {
            // Check if the form state has the breakpoint enabled.
            if (!isset($values['plugin_configuration']['breakpoints'][$breakpoint_name]['enabled'])) {
              continue;
            }
          }
          // Get $properties (fields) from $config.
          $config_values[$breakpoint_name] = $breakpoint_settings;

        }
      }
      if (!empty($config_values)) {
        $values['plugin_configuration']['breakpoints'] = NestedArray::mergeDeepArray([
          $values['plugin_configuration']['breakpoints'],
          $config_values,
        ]);
      }

      $entity->set('plugin_configuration', $values['plugin_configuration']);
    }
  }

  /**
   * Get plugin configuration.
   *
   * @param string $pluginId
   *   The plugin id.
   * @param array $formField
   *   The form field.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The form state.
   *
   * @return array
   *   The array of configuration.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  protected function getPluginConfiguration(string $pluginId, array $formField, FormStateInterface $formState) : array {
    /** @var \Drupal\breezy_paragraphs\Entity\BreezyParagraphsVariantInterface $variant */
    $variant = $this->entity;
    $configuration = ['_entity' => $variant->id()];
    /** @var \Drupal\breezy_paragraphs\Plugin\BreezyParagraphs\BehaviorVariant\BehaviorVariantInterface $plugin */
    $plugin = $this->variantPluginManager->createInstance($pluginId, $configuration);

    $plugin->submitConfigurationForm($formField, $formState);

    return $plugin->getConfiguration();
  }

  /**
   * Get variant plugin options.
   *
   * @return array
   *   An array of variant plugin options.
   */
  protected function getVariantPluginOptions() {
    $variant_plugin_options = [];
    $variant_plugins = $this->variantPluginManager->getValidDefinitions();
    foreach ($variant_plugins as $id => $definition) {
      $variant_plugin_options[$id] = $definition['label'];
    }
    return $variant_plugin_options;
  }

  /**
   * Plugin id callback.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The portion of the form to return.
   */
  public function pluginIdCallback(array &$form, FormStateInterface $form_state) {
    return $form['plugin_configuration'];
  }

}
