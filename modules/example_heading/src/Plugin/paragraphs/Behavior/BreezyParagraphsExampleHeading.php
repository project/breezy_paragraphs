<?php

namespace Drupal\breezy_paragraphs_example_heading\Plugin\paragraphs\Behavior;

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\paragraphs\Entity\ParagraphsType;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\paragraphs\ParagraphsBehaviorBase;

/**
 * Provides an BreezyParagraphsExampleHeading behavior plugin.
 *
 * @ParagraphsBehavior(
 *   id = "breezy_paragraphs_example_heading",
 *   label = @Translation("Breezy Paragraphs Example Heading"),
 *   description = @Translation("Provides Example Heading Paragraph
 *   behaviors."),
 *   weight = -1,
 * )
 */
class BreezyParagraphsExampleHeading extends ParagraphsBehaviorBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'heading_levels' => [
        2 => 'H2',
        3 => 'H3',
        4 => 'H4',
        5 => 'H5',
        6 => 'H6',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(ParagraphsType $paragraphs_type) {
    if ($paragraphs_type->id() == 'example_heading') {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['heading_levels'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Available heading levels'),
      '#options' => $this->defaultConfiguration()['heading_levels'],
      '#default_value' => $this->configuration['heading_levels'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $heading_levels = $form_state->getValue('heading_levels');
    $this->configuration['heading_levels'] = $heading_levels;
  }

  /**
   * {@inheritdoc}
   */
  public function buildBehaviorForm(ParagraphInterface $paragraph, array &$form, FormStateInterface $form_state) {
    $form = parent::buildBehaviorForm($paragraph, $form, $form_state);
    $form['heading_level'] = [
      '#type' => 'select',
      '#title' => $this->t('Heading level'),
      '#description' => $this->t('Select the heading level.'),
      '#options' => array_intersect_key($this->defaultConfiguration()['heading_levels'], $this->configuration['heading_levels']),
      '#default_value' => $paragraph->getBehaviorSetting($this->getPluginId(), 'heading_level', '2'),
      '#required' => TRUE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function preprocess(&$variables) {
    $paragraph = $variables['paragraph'];
    $heading_level = $paragraph->getBehaviorSetting($this->getPluginId(), 'heading_level', '2');
    $variables['heading_level'] = $heading_level;
  }

  /**
   * {@inheritdoc}
   */
  public function view(array &$build, Paragraph $paragraph, EntityViewDisplayInterface $display, $view_mode) {}

}
