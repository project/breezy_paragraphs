# Breezy Paragraphs: Example Text

This is an example of how to create a "text" Paragraph type.  It also includes a text editor style that demonstrates a
way to add TailwindCSS classes to an element.

## Installed configurations

Paragraph: Example Text
Editor: Example HTML
Field: Example Text - Long formatted text

## Additional

Adds "Prose" TailwindCSS property and classes.

## Usage

Ensure the Tailwind Typography plugin is active.  If you are using the CDN, you can simply add `?plugins=typography`.

Attach the new Text paragraph type to an entity.



