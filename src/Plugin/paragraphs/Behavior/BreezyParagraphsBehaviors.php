<?php

namespace Drupal\breezy_paragraphs\Plugin\paragraphs\Behavior;

use Drupal\breakpoint\BreakpointManagerInterface;
use Drupal\breezy_paragraphs\Service\VariantManagerInterface;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\paragraphs\Entity\ParagraphsType;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\paragraphs\ParagraphsBehaviorBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides BreezyParagraphs behavior plugin.
 *
 * @ParagraphsBehavior(
 *   id = "breezy_paragraphs",
 *   label = @Translation("Breezy Paragraphs"),
 *   description = @Translation("Integrates paragraphs with Breezy Utility
 *   classes."), weight = 0
 * )
 */
class BreezyParagraphsBehaviors extends ParagraphsBehaviorBase {

  /**
   * Config Breakpoints.
   *
   * @var array
   */
  protected array $breakpoints;

  /**
   * Drupal\breakpoint\BreakpointManagerInterface definition.
   *
   * @var \Drupal\breakpoint\BreakpointManagerInterface
   */
  protected BreakpointManagerInterface $breakpointManager;

  /**
   * BehaviorVariantManagerInterface definition.
   *
   * @var \Drupal\breezy_paragraphs\Service\VariantManagerInterface
   */
  protected VariantManagerInterface $variantManager;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Constructs a new behavior plugin object.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin id.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   Entity field manager service.
   * @param \Drupal\breakpoint\BreakpointManagerInterface $breakpoint_manager
   *   The breakpoint manager service.
   * @param \Drupal\breezy_paragraphs\Service\VariantManagerInterface $variant_manager
   *   The variant manager service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(array $configuration, string $plugin_id, mixed $plugin_definition, EntityFieldManagerInterface $entity_field_manager, BreakpointManagerInterface $breakpoint_manager, VariantManagerInterface $variant_manager, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_field_manager);
    $this->breakpointManager = $breakpoint_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->variantManager = $variant_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @var \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager */
    $entity_field_manager = $container->get('entity_field.manager');
    /** @var \Drupal\breakpoint\BreakpointManagerInterface $breakpoint_manager */
    $breakpoint_manager = $container->get('breakpoint.manager');
    /** @var \Drupal\breezy_paragraphs\Service\VariantManagerInterface $variant_manager */
    $variant_manager = $container->get('breezy_paragraphs.variant.manager');
    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager */
    $entity_type_manager = $container->get('entity_type.manager');
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $entity_field_manager,
      $breakpoint_manager,
      $variant_manager,
      $entity_type_manager
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'behavior_variant' => '',
      'variant_settings' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(ParagraphsType $paragraphs_type) {
    $variants = \Drupal::service('breezy_paragraphs.variant.manager')->getVariantOptionsForParagraphType($paragraphs_type);
    if (!empty($variants)) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form_obj = $form_state->getFormObject();
    /** @var \Drupal\paragraphs\Entity\ParagraphsType $paragraph_type */
    $paragraph_type = $form_obj->getEntity();

    $variant_options = $this->variantManager->getVariantOptionsForParagraphType($paragraph_type);
    if (!empty($variant_options)) {
      $form['behavior_variant'] = [
        '#type' => 'select',
        '#title' => $this->t('Select behavior variant'),
        '#options' => $variant_options,
        '#default_value' => $this->configuration['behavior_variant'] ?? '',
        '#empty_option' => $this->t('-- Select --'),
      ];
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $variant_type = $form_state->getValue('behavior_variant');
    $this->configuration['behavior_variant'] = $variant_type;

  }

  /**
   * {@inheritdoc}
   */
  public function buildBehaviorForm(ParagraphInterface $paragraph, array &$form, FormStateInterface $form_state) {

    if (!empty($this->configuration['behavior_variant'])) {
      $behavior_variant_id = $this->configuration['behavior_variant'];
      $variant = $this->variantManager->getVariantById($behavior_variant_id);
      $behavior_settings = $paragraph->getAllBehaviorSettings();

      $form_state->set('variant', $variant);

      $input = $form_state->getUserInput();
      // Prefer the current input.
      if (isset($input['variant_settings'])) {
        $variant_settings = $input['variant_settings'];
      }
      // If there are saved behavior settings, choose those next.
      elseif (isset($behavior_settings['breezy_paragraphs']['variant_settings'])) {
        $variant_settings = $behavior_settings['breezy_paragraphs']['variant_settings'];
      }
      // If all else fails, get the current configuration.
      else {
        $variant_settings = $this->configuration['variant_settings'];
      }

      $form_state->set('default_settings', $variant_settings);

      $form['variant_settings_wrapper'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Settings'),
        '#attributes' => [
          'id' => 'variant-settings-wrapper',
        ],
      ];
      $form['variant_settings_wrapper']['variant_settings'] = [
        '#parents' => array_values(array_merge($form['#parents'], ['variant_settings'])),
      ];
      $form['variant_settings_wrapper']['variant_settings'] = $variant->buildBehaviorForm($form['variant_settings_wrapper']['variant_settings'], $form_state);
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function view(array &$build, Paragraph $paragraph, EntityViewDisplayInterface $display, $view_mode) {

    if (!empty($this->configuration['behavior_variant'])) {
      $behavior_variant_id = $this->configuration['behavior_variant'];
      $variant = $this->variantManager->getVariantById($behavior_variant_id);
      $behavior_settings = $paragraph->getAllBehaviorSettings();
      $variant_settings = [];
      if (isset($behavior_settings['breezy_paragraphs']['variant_settings'])) {
        $variant_settings = $behavior_settings['breezy_paragraphs']['variant_settings'];
      }
      $variant->view($build, $paragraph, $display, $view_mode, $variant_settings);
    }

  }

}
