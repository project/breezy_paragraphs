<?php

namespace Drupal\breezy_paragraphs_ui\Form;

use Drupal\breezy_paragraphs\Entity\BreezyParagraphsVariantInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Provides an element edit form.
 */
class BreezyParagraphsElementEditForm extends BreezyParagraphsElementFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'breezy_paragraphs_ui_element_edit_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, BreezyParagraphsVariantInterface $breezy_paragraphs_variant = NULL, $key = NULL, $parent_key = NULL, $type = NULL) {
    if (!$parent_key) {
      $parent_key = $this->getRequest()->query->get('parent');
    }
    $parent_array = Json::decode($parent_key);
    if (!$key) {
      $key = $this->getRequest()->query->get('key');
    }
    $this->key = $key;
    $this->element = $breezy_paragraphs_variant->getElementConfiguration($parent_array, $key);
    if ($this->element === NULL) {
      throw new NotFoundHttpException();
    }
    $this->element['#type'] = $type;

    $form['#title'] = $this->t('Edit element');

    $form = parent::buildForm($form, $form_state, $breezy_paragraphs_variant, $key, $parent_key, $type);
    return $form;
  }

}
