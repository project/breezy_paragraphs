## INTRODUCTION

Breezy Paragraphs provides a Paragraphs Behavior plugin that allows site builders to apply utility classes provided by
[Breezy Utility](https://www.drupal.org/project/breezy_utility) to Paragraphs elements, and optionally expose class
options to editors.



## REQUIREMENTS

[Paragraphs](https://www.drupal.org/project/paragraphs)
[Breezy Utility](https://www.drupal.org/project/breezy_utility)


