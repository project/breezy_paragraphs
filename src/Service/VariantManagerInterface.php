<?php

namespace Drupal\breezy_paragraphs\Service;

use Drupal\breezy_paragraphs\Entity\BreezyParagraphsVariantInterface;
use Drupal\paragraphs\Entity\ParagraphsType;

/**
 * Provides an interface for the VariantManager service.
 */
interface VariantManagerInterface {

  /**
   * Get BreezyParagraphsVariant options from paragraph type.
   *
   * @param \Drupal\paragraphs\Entity\ParagraphsType $paragraph_type
   *   The paragraph type.
   *
   * @return array
   *   An array of BreezyParagraphsVariants keyed by id.
   */
  public function getVariantOptionsForParagraphType(ParagraphsType $paragraph_type) : array;

  /**
   * Get variant from variant id.
   *
   * @param string $variant_id
   *   The variant id.
   *
   * @return \Drupal\breezy_paragraphs\Entity\BreezyParagraphsVariantInterface
   *   The paragraphs variant entity interface.
   */
  public function getVariantById(string $variant_id): BreezyParagraphsVariantInterface;

}
