<?php

namespace Drupal\breezy_paragraphs\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\paragraphs\Entity\Paragraph;

/**
 * Provides an interface for BreezyParagraphsVariant entities.
 */
interface BreezyParagraphsVariantInterface extends ConfigEntityInterface {

  /**
   * Get plugin id.
   *
   * @return string
   *   The plugin id.
   */
  public function getPluginId(): string;

  /**
   * Set plugin id.
   *
   * @param string $plugin_id
   *   The plugin id.
   *
   * @return \Drupal\breezy_paragraphs\Entity\BreezyParagraphsVariant
   *   The variant object.
   */
  public function setPluginId(string $plugin_id): BreezyParagraphsVariant;

  /**
   * Get the BreezyParagraphsVariant plugin configuration.
   *
   * @return mixed
   *   The plugin configuration.
   */
  public function getPluginConfiguration();

  /**
   * Set the BreezyParagraphsVariant plugin configuration.
   *
   * @param array $configuration
   *   The plugin configuration array.
   *
   * @return \Drupal\breezy_paragraphs\Entity\BreezyParagraphsVariant
   *   The variant object.
   */
  public function setPluginConfiguration(array $configuration);

  /**
   * Returns the status of the entity.
   *
   * @return bool
   *   Whether this behavior entity is enabled.
   */
  public function isEnabled(): bool;

  /**
   * Set the status of the entity.
   *
   * @param bool $enabled
   *   The new status.
   *
   * @return \Drupal\breezy_paragraphs\Entity\BreezyParagraphsVariant
   *   The variant object.
   */
  public function setEnabled(bool $enabled);

  /**
   * Gets the paragraph type this entity will work on.
   *
   * @return string
   *   The paragraph type (bundle).
   */
  public function getParagraphType(): string;

  /**
   * Set the paragraph type this entity will work on.
   *
   * @param string $paragraph_type
   *   The paragraph type (bundle).
   *
   * @return \Drupal\breezy_paragraphs\Entity\BreezyParagraphsVariant
   *   The variant object.
   */
  public function setParagraphType(string $paragraph_type);

  /**
   * Set breakpoint group.
   *
   * @param string $breakpoint_group
   *   The breakpoint group.
   *
   * @return \Drupal\breezy_paragraphs\Entity\BreezyParagraphsVariant
   *   The variant object.
   */
  public function setBreakpointGroup(string $breakpoint_group);

  /**
   * Get breakpoint group.
   *
   * @return string
   *   The breakpoint group name.
   */
  public function getBreakpointGroup(): string;

  /**
   * Get element configuration.
   *
   * @param array $parent_key
   *   The element parent key.
   * @param mixed $key
   *   The element form key.
   *
   * @return array|null
   *   An array containing an initialized element.
   */
  public function getElementConfiguration(array $parent_key, string $key);

  /**
   * Get enabled properties.
   *
   * @return array
   *   An array of enabled properties, keyed by breakpoint.
   */
  public function getEnabledProperties() : array;

  /**
   * Set element properties.
   *
   * @param string $key
   *   The element key.
   * @param array $properties
   *   The element properties.
   * @param array $parent_key
   *   The parent key.
   *
   * @return $this
   *   The BreezyParagraphsVariant entity.
   */
  public function setElementProperties(string $key, array $properties, array $parent_key = []);

  /**
   * Delete element.
   *
   * @param string $key
   *   The element key.
   * @param array $parent_key
   *   The element parent.
   */
  public function deleteElement(string $key, array $parent_key);

  /**
   * Build behavior form.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return array
   *   An array of form elements.
   */
  public function buildBehaviorForm(array $form, FormStateInterface $form_state): array;

  /**
   * Build paragraph classes.
   *
   * @param array $behavior_form_settings
   *   The settings submitted from the behavior form.
   *
   * @return array
   *   An array of classes keyed by the element.
   */
  public function buildParagraphClasses(array $behavior_form_settings): array;

  /**
   * Builds the paragraph render array.
   *
   * @param array &$build
   *   A renderable array representing the paragraph. The module may add
   *   elements to $build prior to rendering. The structure of $build is a
   *   renderable array as expected by drupal_render().
   * @param \Drupal\paragraphs\Entity\Paragraph $paragraph
   *   The paragraph.
   * @param \Drupal\Core\Entity\Display\EntityViewDisplayInterface $display
   *   The entity view display holding the display options configured for the
   *   entity components.
   * @param string $view_mode
   *   The view mode the entity is rendered in.
   * @param array $behavior_form_settings
   *   The variant settings from the behavior form.
   */
  public function view(array &$build, Paragraph $paragraph, EntityViewDisplayInterface $display, string $view_mode, array $behavior_form_settings);

}
