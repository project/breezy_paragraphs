<?php

namespace Drupal\breezy_paragraphs;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;

/**
 * Provides a list builder for BreezyParagraphsVariant entities.
 */
class BreezyParagraphsVariantListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    return [
      'label' => $this->t('Label'),
      'id' => $this->t('Machine name'),
      'paragraph_type' => $this->t('Paragraph type'),
      'status' => $this->t('Status'),
    ] + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    /** @var \Drupal\breezy_paragraphs\Entity\BreezyParagraphsVariantInterface $entity */

    $row = [
      'label' => $entity->label(),
      'id' => $entity->id(),
      'paragraph_type' => $entity->getParagraphType(),
      'status' => $entity->status(),

    ];

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity): array {
    /** @var \Drupal\breezy_paragraphs\Entity\BreezyParagraphsVariantInterface $entity */

    $operations = parent::getDefaultOperations($entity);
    return $operations;
  }

  /**
   * {@inheritdoc}
   */
  protected function ensureDestination(Url $url) {
    // Never add a destination to operation URLs.
    return $url;
  }

}
