(function ($, Drupal, once) {
  /**
   * Click handler behavior buttons.
   *
   * @type {Object}
   */
  Drupal.behaviors.breezyParagraphsBehaviorsToggle = {
    attach(context) {
      once(
        'add-click-handler',
        $('.js-paragraphs-button-behaviors'),
        context,
      ).forEach((element) => {
        const $button = $(element);
        const $paragraphsWidget = $button.closest('.paragraph-top').parent();

        $button.addClass('content-active');
        $paragraphsWidget.addClass('content-active');

        $button.on('click', (event) => {
          const $trigger = $(event.target);

          const $currentParagraphsWidget = $trigger
            .closest('.paragraph-top')
            .parent();

          if ($currentParagraphsWidget.hasClass('content-active')) {
            $trigger.removeClass('content-active').addClass('behavior-active');

            $currentParagraphsWidget.find('.paragraphs-behavior').show();
            $currentParagraphsWidget.find('.paragraphs-subform').hide();
            $currentParagraphsWidget
              .removeClass('content-active')
              .addClass('behavior-active');
          } else {
            $trigger.removeClass('behavior-active').addClass('content-active');
            $currentParagraphsWidget.find('.paragraphs-behavior').hide();
            $currentParagraphsWidget.find('.paragraphs-subform').show();
            $currentParagraphsWidget
              .removeClass('behavior-active')
              .addClass('content-active');
          }

          event.preventDefault();

          return false;
        });
      });
    },
  };
})(jQuery, Drupal, once);
