<?php

namespace Drupal\breezy_paragraphs_example_text\Plugin\BreezyParagraphs\BehaviorVariant;

use Drupal\breezy_paragraphs\Attribute\BehaviorVariant;
use Drupal\breezy_paragraphs\Plugin\BreezyParagraphs\BehaviorVariant\BehaviorVariantBase;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\paragraphs\Entity\Paragraph;

/**
 * Provides an example text paragraph variant plugin.
 */
#[BehaviorVariant(
  id: 'example_text',
  label: new TranslatableMarkup("Example text"),
  description: new TranslatableMarkup("Example text paragraph behavior variant."),
  paragraph_type: 'example_text',
  paragraph_elements: [
    'text_wrapper' => new TranslatableMarkup("Text wrapper"),
  ],
)]
class ExampleText extends BehaviorVariantBase {

  /**
   * {@inheritdoc}
   */
  public function view(array &$build, Paragraph $paragraph, EntityViewDisplayInterface $display, string $view_mode, array $behavior_form_settings, array $variant_classes) {

    if (isset($variant_classes['text_wrapper'])) {
      if (isset($build['#attributes']['class'])) {
        $build['#attributes']['class'] += $variant_classes['text_wrapper'];
      }
      else {
        $build['#attributes'] = [
          'class' => $variant_classes['text_wrapper'],
        ];
      }
    }

  }

}
