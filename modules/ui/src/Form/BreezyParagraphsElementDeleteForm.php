<?php

namespace Drupal\breezy_paragraphs_ui\Form;

use Drupal\breezy_paragraphs\Entity\BreezyParagraphsVariantInterface;
use Drupal\breezy_utility\BreezyUtilityElementPluginManagerInterface;
use Drupal\breezy_utility\Form\BreezyUtilityDeleteFormBase;
use Drupal\breezy_utility\Plugin\BreezyUtility\Element\BreezyUtilityElementInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Provides a form for deleting elements.
 */
class BreezyParagraphsElementDeleteForm extends BreezyUtilityDeleteFormBase {

  /**
   * BreezyUtilityElementPluginManagerInterface definition.
   *
   * @var \Drupal\breezy_utility\BreezyUtilityElementPluginManagerInterface
   */
  protected BreezyUtilityElementPluginManagerInterface $elementManager;

  /**
   * The BreezyParagraphsVariant containing the element to be deleted.
   *
   * @var \Drupal\breezy_paragraphs\Entity\BreezyParagraphsVariantInterface
   */
  protected BreezyParagraphsVariantInterface $variant;

  /**
   * The element to be deleted.
   *
   * @var \Drupal\breezy_utility\Plugin\BreezyUtility\Element\BreezyUtilityElementInterface
   */
  protected BreezyUtilityElementInterface $elementPlugin;

  /**
   * The element key.
   *
   * @var string
   */
  protected string $key;

  /**
   * The parent key.
   *
   * @var array
   */
  protected array $parentKey;

  /**
   * The element.
   *
   * @var array
   */
  protected array $element;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    /** @var \Drupal\breezy_utility\BreezyUtilityElementPluginManagerInterface $element_manager */
    $element_manager = $container->get('plugin.manager.breezy_utility.element');
    $instance->elementManager = $element_manager;
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'breezy_paragraphs_ui_element_delete_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Delete the %element element from the %variant', [
      '%element' => $this->getElementTitle(),
      '%variant' => $this->variant->label(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {

    $element = $this->element;
    return [
      'title' => [
        '#markup' => $this->t('This will delete %element.', [
          '%element' => $element['element']['title'] ?? 'this element',
        ]),
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return $this->variant->toUrl('edit-form');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, BreezyParagraphsVariantInterface $breezy_paragraphs_variant = NULL, $type = NULL, $key = NULL, $parent_key = NULL) {
    $this->variant = $breezy_paragraphs_variant;
    if (!$parent_key) {
      $parent_key = $this->getRequest()->query->get('parent');
    }
    $parent_array = Json::decode($parent_key);
    $this->parentKey = $parent_array;
    if (!$key) {
      $key = $this->getRequest()->query->get('key');
    }
    $this->key = $key;
    $element = $breezy_paragraphs_variant->getElementConfiguration($parent_array, $key);
    if ($element === NULL) {
      $this->element = [];
      throw new NotFoundHttpException();
    }
    else {
      $this->element = $element;
    }

    $form = parent::buildForm($form, $form_state);
    $form = $this->buildDialogConfirmForm($form, $form_state);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->variant->deleteElement($this->key, $this->parentKey);
    $this->variant->save();

    $query = [];
    $form_state->setRedirectUrl($this->variant->toUrl('edit-form', ['query' => $query]));
  }

}
