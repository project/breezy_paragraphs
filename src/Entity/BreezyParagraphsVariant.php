<?php

namespace Drupal\breezy_paragraphs\Entity;

use Drupal\breezy_paragraphs\Plugin\BreezyParagraphs\BehaviorVariant\BehaviorVariantInterface;
use Drupal\breezy_utility\Utility\BreezyUtilityElementHelper;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\paragraphs\Entity\Paragraph;

/**
 * Defines the BreezyParagraphsVariant config entity.
 *
 * @ConfigEntityType(
 *   id = "breezy_paragraphs_variant",
 *   label = @Translation("Breezy Paragraphs Variant"),
 *   label_collection = @Translation("Breezy Paragraphs Variants"),
 *   label_plural = @Translation("Breezy Paragraphs Variants"),
 *   label_count = @PluralTranslation(
 *     singular = "@count variant",
 *     plural = "@count variants",
 *   ),
 *   admin_permission = "administer breezy paragraphs variants",
 *   config_prefix = "breezy_paragraphs_variant",
 *   handlers = {
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "storage" = "Drupal\breezy_paragraphs\Storage\BreezyParagraphsVariantStorage",
 *     "list_builder" = "Drupal\breezy_paragraphs\BreezyParagraphsVariantListBuilder",
 *     "form" = {},
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "status" = "status",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "status",
 *     "paragraph_type",
 *     "plugin_id",
 *     "plugin_configuration",
 *   },
 *   links = {
 *     "collection" = "/admin/config/content/breezy/paragraphs/variants",
 *     "edit-form" = "/admin/config/content/breezy/paragraphs/variants/{breezy_paragraphs_variant}/edit",
 *     "duplicate-form" = "/admin/config/content/breezy/paragraphs/variants/{breezy_paragraphs_variant}/duplicate",
 *     "delete-form" = "/admin/config/content/breezy/paragraphs/variants/{breezy_paragraphs_variant}/delete",
 *   },
 * )
 */
class BreezyParagraphsVariant extends ConfigEntityBase implements BreezyParagraphsVariantInterface {

  /**
   * The variant id.
   *
   * @var string
   */
  protected string $id;

  /**
   * The variant label.
   *
   * @var string
   */
  protected string $label;

  /**
   * The variant status.
   *
   * @var bool
   */
  protected $status;

  /**
   * The breakpoint group.
   *
   * @var string
   */
  protected string $breakpointGroup = '';

  /**
   * The paragraph type (bundle).
   *
   * @var string
   */
  protected string $paragraph_type = '';

  /**
   * The plugin ID.
   *
   * @var string
   */
  protected string $plugin_id = '';

  /**
   * The plugin configuration.
   *
   * @var array
   */
  protected array $plugin_configuration = [];

  /**
   * {@inheritdoc}
   */
  public function getPluginId(): string {
    return $this->plugin_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setPluginId(string $plugin_id): BreezyParagraphsVariant {
    $this->plugin_id = $plugin_id;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginConfiguration() {
    return $this->plugin_configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setPluginConfiguration(array $configuration) {
    $this->plugin_configuration = $configuration;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled(): bool {
    return $this->status;
  }

  /**
   * {@inheritdoc}
   */
  public function setEnabled(bool $enabled) {
    $this->status = $enabled;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getParagraphType(): string {
    return $this->paragraph_type;
  }

  /**
   * {@inheritdoc}
   */
  public function setParagraphType(string $paragraph_type) {
    $this->paragraph_type = $paragraph_type;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setBreakpointGroup(string $breakpoint_group) {
    $this->breakpointGroup = $breakpoint_group;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getBreakpointGroup(): string {
    return $this->breakpointGroup;
  }

  /**
   * {@inheritdoc}
   */
  public function getElementConfiguration(array $parent_key, string $key) {
    // Find the element configuration based on the key.
    $variant_plugin_configuration = $this->getPluginConfiguration();
    $existing_properties = NestedArray::getValue($variant_plugin_configuration, $parent_key);

    return $existing_properties[$key] ?? NULL;

  }

  /**
   * {@inheritdoc}
   */
  public function getEnabledProperties() : array {
    $plugin_configuration = $this->getPluginConfiguration();
    $enabled_properties = [];
    if (!isset($plugin_configuration['breakpoints'])) {
      return $enabled_properties;
    }
    foreach ($plugin_configuration['breakpoints'] as $breakpoint_name => $breakpoint_sections) {
      if ($breakpoint_sections['enabled']) {
        $enabled_properties['breakpoints'][$breakpoint_name] = $breakpoint_sections;
        unset($enabled_properties['breakpoints'][$breakpoint_name]['enabled']);
      }
    }
    return $enabled_properties;

  }

  /**
   * {@inheritdoc}
   */
  public function setElementProperties(string $key, array $properties, array $parent_key = []) {

    if (empty($parent_key)) {
      return $this;
    }
    // Get variant plugin, determine where the $key and $parent_key goes, inject
    // the new element, set its properties.
    $plugin_configuration = $this->getPluginConfiguration();
    if (!$plugin_configuration) {
      $plugin_configuration = [];
    }
    $existing_properties = NestedArray::getValue($plugin_configuration, $parent_key);
    if (is_array($existing_properties)) {
      $existing_properties[$key] = $properties;
    }
    else {
      $existing_properties = [$key => $properties];
    }
    NestedArray::setValue($plugin_configuration, $parent_key, $existing_properties);
    $this->setPluginConfiguration($plugin_configuration);
    return $this;
  }

  /**
   * Set element properties.
   *
   * @param array $elements
   *   An associative nested array of elements.
   * @param string $key
   *   The element's key.
   * @param array $properties
   *   An associative array of properties.
   * @param array $parent_key
   *   (optional) The element's parent key. Only used for new elements.
   *
   * @return bool
   *   TRUE when the element's properties has been set. FALSE when the element
   *   has not been found.
   */
  protected function setElementPropertiesRecursive(array &$elements, string $key, array $properties, array $parent_key = []) {
    foreach ($elements as $element_key => &$element) {
      // Make sure the element key is a string.
      $element_key = (string) $element_key;

      if (!BreezyUtilityElementHelper::isElement($element, $element_key)) {
        continue;
      }

      if ($element_key === $key) {
        $element = $properties + BreezyUtilityElementHelper::removeProperties($element);
        return TRUE;
      }

      if ($element_key === $parent_key) {
        $element[$key] = $properties;
        return TRUE;
      }

      if ($this->setElementPropertiesRecursive($element, $key, $properties, $parent_key)) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteElement(string $key, array $parent_key) {
    array_push($parent_key, $key);
    $configuration = $this->getPluginConfiguration();
    NestedArray::unsetValue($configuration, $parent_key, $key);
    $this->setPluginConfiguration($configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function buildBehaviorForm(array $form, FormStateInterface $form_state): array {
    $form['#tree'] = TRUE;
    $variant_configuration = $this->getPluginConfiguration();
    if (empty($variant_configuration)) {
      return $form;
    }

    $variant_plugin = $this->getVariantPlugin();

    $form_state->set('breakpoint_group', $this->getBreakpointGroup());
    $form = $variant_plugin->getBehaviorForm($form, $form_state);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function buildParagraphClasses(array $behavior_form_settings): array {
    $variant_configuration = $this->getPluginConfiguration();
    if (empty($variant_configuration)) {
      return [];
    }
    $variant_plugin = $this->getVariantPlugin($variant_configuration);
    if (!$variant_plugin) {
      return [];
    }
    return $variant_plugin->buildBehaviorClasses($behavior_form_settings);
  }

  /**
   * Get variant plugin.
   *
   * @param array|null $plugin_configuration
   *   (optional) If not provided, default variant entity config is used.
   *
   * @return \Drupal\breezy_paragraphs\Plugin\BreezyParagraphs\BehaviorVariant\BehaviorVariantInterface
   *   The variant plugin instance.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  protected function getVariantPlugin(array $plugin_configuration = NULL): BehaviorVariantInterface {
    if (!$plugin_configuration) {
      $plugin_configuration = $this->getPluginConfiguration();
    }
    /** @var \Drupal\breezy_paragraphs\Service\BreezyParagraphsBehaviorVariantPluginManagerInterface $variant_manager */
    $variant_manager = \Drupal::service('plugin.manager.breezy_paragraphs.behavior_variant');
    /** @var \Drupal\breezy_paragraphs\Plugin\BreezyParagraphs\BehaviorVariant\BehaviorVariantInterface */
    return $variant_manager->createInstance($this->getPluginId(), $plugin_configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function view(array &$build, Paragraph $paragraph, EntityViewDisplayInterface $display, string $view_mode, array $behavior_form_settings) {
    $variant_classes = $this->buildParagraphClasses($behavior_form_settings);
    $variant_configuration = $this->getPluginConfiguration();
    if (empty($variant_configuration)) {
      return [];
    }
    $plugin = $this->getVariantPlugin($variant_configuration);
    $plugin->view($build, $paragraph, $display, $view_mode, $behavior_form_settings, $variant_classes);
  }

}
