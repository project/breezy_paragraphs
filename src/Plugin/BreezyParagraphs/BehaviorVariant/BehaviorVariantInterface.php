<?php

namespace Drupal\breezy_paragraphs\Plugin\BreezyParagraphs\BehaviorVariant;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\paragraphs\Entity\Paragraph;

/**
 * Provides an interface for BehaviorVariant plugins.
 */
interface BehaviorVariantInterface extends ConfigurableInterface, ContainerFactoryPluginInterface, PluginFormInterface {

  /**
   * Retrieves the plugin's label.
   *
   * @return string
   *   The plugin's human-readable and translated label.
   */
  public function label() : string;

  /**
   * Retrieves the plugin's description.
   *
   * @return string
   *   The plugin's translated description; or empty string if it has none.
   */
  public function getDescription() : string;

  /**
   * Get plugin paragraph type.
   *
   * @return string
   *   The paragraph type from the plugin definition.
   */
  public function getParagraphType() : string;

  /**
   * Get paragraph elements.
   *
   * @return array
   *   An array of paragraph elements from the plugin definition.
   */
  public function getParagraphElements(): array;

  /**
   * Get theme function.
   *
   * The theme function is used for the $build in the view method.
   *
   * @return string|null
   *   A theme function name, if defined in the plugin.
   */
  public function getThemeFunction(): string|NULL;

  /**
   * Get the paragraph behavior form.
   *
   * @param array $form
   *   The behavior form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return array
   *   The array representing the behavior form.
   */
  public function getBehaviorForm(array $form, FormStateInterface $form_state): array;

  /**
   * Builds the paragraph classes.
   *
   * @param array $behavior_form_settings
   *   The paragraph behavior form values.
   *
   * @return array
   *   An array of classes for the paragraph.
   */
  public function buildBehaviorClasses(array $behavior_form_settings): array;

  /**
   * Builds the paragraph render array.
   *
   * @param array &$build
   *   A renderable array representing the paragraph. The module may add
   *   elements to $build prior to rendering. The structure of $build is a
   *   renderable array as expected by drupal_render().
   * @param \Drupal\paragraphs\Entity\Paragraph $paragraph
   *   The paragraph.
   * @param \Drupal\Core\Entity\Display\EntityViewDisplayInterface $display
   *   The entity view display holding the display options configured for the
   *   entity components.
   * @param string $view_mode
   *   The view mode the entity is rendered in.
   * @param array $behavior_form_settings
   *   The variant settings from the behavior form.
   * @param array $variant_classes
   *   The classes array, keyed by paragraph_elements.
   */
  public function view(array &$build, Paragraph $paragraph, EntityViewDisplayInterface $display, string $view_mode, array $behavior_form_settings, array $variant_classes);

}
