<?php

declare(strict_types=1);

namespace Drupal\breezy_paragraphs\Service;

use Drupal\breezy_paragraphs\Attribute\BehaviorVariant;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Provides a manager for BehaviorVariant plugins.
 */
class BreezyParagraphsBehaviorVariantPluginManager extends DefaultPluginManager implements BreezyParagraphsBehaviorVariantPluginManagerInterface {

  use DependencySerializationTrait;

  /**
   * Drupal\Core\Entity\EntityTypeBundleInfoInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected EntityTypeBundleInfoInterface $bundleInfo;

  /**
   * Constructs a new BreezyParagraphsBehaviorVariantPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $bundle_info
   *   The bundle info service.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler, EntityTypeBundleInfoInterface $bundle_info) {
    parent::__construct('Plugin/BreezyParagraphs/BehaviorVariant', $namespaces, $module_handler, 'Drupal\breezy_paragraphs\Plugin\BreezyParagraphs\BehaviorVariant\BehaviorVariantInterface', BehaviorVariant::class);
    $this->setCacheBackend($cache_backend, 'breezy_paragraphs_behavior_variant');
    $this->alterInfo('breezy_paragraphs_behavior_variant');
    $this->bundleInfo = $bundle_info;
  }

  /**
   * {@inheritdoc}
   */
  public function validateRequirements(string $plugin_id): bool {
    $plugin = $this->getDefinition($plugin_id);
    if (isset($plugin['paragraph_type'])) {
      if (array_key_exists($plugin['paragraph_type'], $this->bundleInfo->getBundleInfo('paragraph'))) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getValidDefinitions(): array {
    $definitions = [];
    foreach ($this->getDefinitions() as $id => $definition) {
      if ($this->validateRequirements($id)) {
        $definitions[$id] = $definition;
      }
    }
    return $definitions;
  }

  /**
   * {@inheritdoc}
   */
  public function getParagraphType(string $plugin_id): string {
    $definition = $this->getDefinition($plugin_id);
    return $definition['paragraph_type'];
  }

  /**
   * {@inheritdoc}
   */
  public function getParagraphElements(string $plugin_id): array {
    $definition = $this->getDefinition($plugin_id);
    return $definition['paragraph_elements'];
  }

}
