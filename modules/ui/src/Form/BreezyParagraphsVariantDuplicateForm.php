<?php

namespace Drupal\breezy_paragraphs_ui\Form;

use Drupal\breezy_paragraphs\Entity\BreezyParagraphsVariantInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for duplicating a BreezyParagraphsBehaviorsVariant.
 */
class BreezyParagraphsVariantDuplicateForm extends FormBase {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new BreezyParagraphsVariant form object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager */
    $entity_type_manager = $container->get('entity_type.manager');
    return new static($entity_type_manager);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'breezy_paragraphs_ui_variant_duplicate_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, BreezyParagraphsVariantInterface $breezy_paragraphs_variant = NULL) {

    $form_state->set('original_variant', $breezy_paragraphs_variant);

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#machine_name' => [
        'exists' => '\Drupal\breezy_paragraphs\Entity\BreezyParagraphsVariant::load',
      ],
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $original_variant = $form_state->get('original_variant');
    // Convert the original to an array, remove UUID.
    $original_data = $original_variant->toArray();
    if (isset($original_data['uuid'])) {
      unset($original_data['uuid']);
    }
    $values = $form_state->getValues();
    $variant_data = [
      'label' => $values['label'],
      'id' => $values['id'],
    ];
    $new_data = array_merge($original_data, $variant_data);

    $new_variant = $this->entityTypeManager->getStorage('breezy_paragraphs_variant')->create($new_data);
    $new_variant->save();
    $this->messenger()->addStatus($this->t('New variant @variant created.', ['@variant' => $new_variant->label()]));
    $form_state->setRedirect('entity.breezy_paragraphs_variant.edit_form', ['breezy_paragraphs_variant' => $new_variant->id()]);
  }

}
