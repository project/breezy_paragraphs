<?php

declare(strict_types=1);

namespace Drupal\breezy_paragraphs\Attribute;

use Drupal\Component\Plugin\Attribute\Plugin;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Defines a BehaviorVariant plugin attribute.
 *
 * Plugin Namespace: Plugin\BreezyParagraphs\BehaviorVariant.
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
class BehaviorVariant extends Plugin {

  /**
   * Constructs a BehaviorVariant attribute.
   *
   * @param string $id
   *   The plugin ID.
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup $label
   *   The human-readable name of the behavior variant.
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup|null $description
   *   (optional) A short description of the behavior variant.
   * @param string $paragraph_type
   *   The paragraph bundle the plugin operates on.
   * @param array $paragraph_elements
   *   The paragraph elements which will receive classes.
   */
  public function __construct(
    public readonly string $id,
    public readonly TranslatableMarkup $label,
    public readonly ?TranslatableMarkup $description,
    public readonly string $paragraph_type,
    public readonly array $paragraph_elements,
  ) {}

}
