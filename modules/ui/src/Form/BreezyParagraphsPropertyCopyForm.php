<?php

namespace Drupal\breezy_paragraphs_ui\Form;

use Drupal\breakpoint\BreakpointManagerInterface;
use Drupal\breezy_paragraphs\Entity\BreezyParagraphsVariantInterface;
use Drupal\breezy_paragraphs\Service\BreezyParagraphsBehaviorVariantPluginManagerInterface;
use Drupal\breezy_utility\Form\BreezyUtilityDialogFormTrait;
use Drupal\breezy_utility\Utility\BreezyUtilityBreakpointHelper;
use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a property copy form.
 */
class BreezyParagraphsPropertyCopyForm extends FormBase {

  use BreezyUtilityDialogFormTrait;

  /**
   * Drupal\breakpoint\BreakpointManagerInterface definition.
   *
   * @var \Drupal\breakpoint\BreakpointManagerInterface
   */
  protected BreakpointManagerInterface $breakpointManager;

  /**
   * BreezyParagraphsBehaviorVariantPluginManagerInterface definition.
   *
   * @var \Drupal\breezy_paragraphs\Service\BreezyParagraphsBehaviorVariantPluginManagerInterface
   */
  protected BreezyParagraphsBehaviorVariantPluginManagerInterface $variantPluginManager;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Breezy Utility settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $breezyUtilitySettings;

  /**
   * BreezyParagraphsBehaviorVariantInterface definition.
   *
   * @var \Drupal\breezy_paragraphs\Entity\BreezyParagraphsVariantInterface
   */
  protected BreezyParagraphsVariantInterface $variant;

  /**
   * The element key.
   *
   * @var string
   */
  protected string $key;

  /**
   * The element parent key.
   *
   * @var array
   */
  protected array $parentKey;

  /**
   * Constructs a new BreezyLayoutsPropertyCopyForm object.
   *
   * @param \Drupal\breakpoint\BreakpointManagerInterface $breakpoint_manager
   *   The breakpoint manager service.
   * @param \Drupal\breezy_paragraphs\Service\BreezyParagraphsBehaviorVariantPluginManagerInterface $variant_plugin_manager
   *   The variant plugin manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(BreakpointManagerInterface $breakpoint_manager, BreezyParagraphsBehaviorVariantPluginManagerInterface $variant_plugin_manager, ConfigFactoryInterface $config_factory) {
    $this->breakpointManager = $breakpoint_manager;
    $this->variantPluginManager = $variant_plugin_manager;
    $this->configFactory = $config_factory;
    $this->breezyUtilitySettings = $config_factory->get('breezy_utility.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /** @var \Drupal\breakpoint\BreakpointManagerInterface $breakpoint_manager */
    $breakpoint_manager = $container->get('breakpoint.manager');
    /** @var \Drupal\breezy_paragraphs\Service\BreezyParagraphsBehaviorVariantPluginManagerInterface $variant_plugin_manager */
    $variant_plugin_manager = $container->get('plugin.manager.breezy_paragraphs.behavior_variant');
    /** @var \Drupal\Core\Config\ConfigFactoryInterface $config_factory */
    $config_factory = $container->get('config.factory');
    return new static($breakpoint_manager, $variant_plugin_manager, $config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'breezy_paragraphs_ui_copy_property';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, BreezyParagraphsVariantInterface $breezy_paragraphs_variant = NULL, $key = NULL, $parent_key = NULL) {
    $this->variant = $breezy_paragraphs_variant;
    if (!$parent_key) {
      $parent_key = $this->getRequest()->query->get('parent');
      $parent_key = Json::decode($parent_key);
    }
    $this->parentKey = $parent_key;

    if (!$key) {
      $key = $this->getRequest()->query->get('key');
    }
    $this->key = $key;

    $property_wrapper_id = 'property-wrapper';
    $form['#attributes'] = [
      'id' => $property_wrapper_id,
    ];

    $options = [];
    $enabled_properties = $breezy_paragraphs_variant->getEnabledProperties();
    if (!empty($enabled_properties)) {

      $breakpoint_group = $this->breezyUtilitySettings->get('breakpoint_group');
      $breakpoints = $this->breakpointManager->getBreakpointsByGroup($breakpoint_group);

      // Remove the copied element section from the available sections.
      $current_element_parent = $parent_key;
      // Remove the 'property' member from the array.
      array_pop($current_element_parent);
      NestedArray::unsetValue($enabled_properties, $current_element_parent);

      $variant_plugin_id = $breezy_paragraphs_variant->getPluginId();
      $variant_plugin_definition = $this->variantPluginManager->getDefinition($variant_plugin_id);

      foreach ($enabled_properties['breakpoints'] as $breakpoint_name => $breakpoint_sections) {
        $breakpoint_name_original = BreezyUtilityBreakpointHelper::getOriginalBreakpointName($breakpoint_name);
        $breakpoint_label = $breakpoint_name;
        // If possible, make a nice label for the breakpoint name.
        if (isset($breakpoints[$breakpoint_name_original])) {
          $breakpoint_label = $breakpoints[$breakpoint_name_original]->getLabel();
          $breakpoint_label = $this->t('Breakpoint: @breakpoint', ['@breakpoint' => $breakpoint_label])->render();
        }
        foreach ($breakpoint_sections as $section => $properties) {
          $section_label = $section;
          // If possible, make a nice label for the section name.
          if (array_key_exists($section, $variant_plugin_definition['paragraph_elements'])) {
            $section_label = $variant_plugin_definition['paragraph_elements'][$section];
          }
          $options[$breakpoint_label][$breakpoint_name . '.' . $section] = $section_label;
        }
      }
    }

    if (!empty($options)) {
      $form['warning'] = [
        '#type' => 'inline_template',
        '#template' => '<div class="warning"><h2>{{ warning_heading }}</h2><p>{{ warning_message }}</p></div>',
        '#context' => [
          'warning_heading' => $this->t('Warning'),
          'warning_message' => $this->t('Selecting a location that already contains a property with the same key will overwrite the values for that property.'),
        ],
      ];
      $form['new_parent'] = [
        '#type' => 'select',
        '#title' => $this->t('Select location'),
        '#options' => $options,
        '#description' => $this->t('Copy property to new location.'),
        '#required' => TRUE,
      ];
      $form['actions'] = [
        '#type' => 'actions',
      ];
      $form['actions']['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Copy element'),
      ];
    }
    else {
      $form['warning'] = [
        '#markup' => $this->t('There are no available sections.'),
      ];
    }

    return $this->buildDialogForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    if (!isset($values['new_parent'])) {
      $form_state->setErrorByName('new_parent', $this->t('Please select a new location.'));
    }
    $parent_parts = explode('.', $values['new_parent']);
    if (count($parent_parts) !== 2) {
      $form_state->setErrorByName('new_parent', $this->t('There was a problem the new location.  Please try again.'));
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $breakpoint_name = NULL;
    $section = NULL;
    if (isset($values['new_parent'])) {
      $parent_parts = explode('.', $values['new_parent']);
      $breakpoint_name = $parent_parts[0];
      $section = $parent_parts[1];
    }
    $new_parent_key = [
      'breakpoints',
      $breakpoint_name,
      $section,
      'properties',
    ];

    $properties = $this->variant->getElementConfiguration($this->parentKey, $this->key);

    $this->variant->setElementProperties($this->key, $properties, $new_parent_key);
    $this->variant->save();

    if ($this->requestStack->getCurrentRequest()->query->get('destination')) {
      $redirect_destination = $this->getRedirectDestination();
      $destination = $redirect_destination->get();
      $destination .= (str_contains($destination, '?') ? '&' : '?') . 'update=' . $this->key;
      $redirect_destination->set($destination);
    }

    $query = ['update' => $this->key];
    $form_state->setRedirectUrl($this->variant->toUrl('edit-form', ['query' => $query]));
  }

}
