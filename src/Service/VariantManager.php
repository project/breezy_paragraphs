<?php

namespace Drupal\breezy_paragraphs\Service;

use Drupal\breezy_paragraphs\Entity\BreezyParagraphsVariantInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\paragraphs\Entity\ParagraphsType;

/**
 * Provides a manager class for BreezyParagraphsVariant entities.
 */
class VariantManager implements VariantManagerInterface {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Constructs a new VariantManager object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getVariantOptionsForParagraphType(ParagraphsType $paragraph_type) : array {
    $variant_options = [];
    /** @var \Drupal\breezy_paragraphs\Entity\BreezyParagraphsVariant[] $variants */
    $variants = $this->entityTypeManager->getStorage('breezy_paragraphs_variant')->loadByProperties([
      'paragraph_type' => $paragraph_type->id(),
      'status' => TRUE,
    ]);
    if ($variants) {
      foreach ($variants as $variant) {
        $variant_options[$variant->id()] = $variant->label();
      }
    }
    return $variant_options;
  }

  /**
   * {@inheritdoc}
   */
  public function getVariantById(string $variant_id): BreezyParagraphsVariantInterface {
    /** @var \Drupal\breezy_paragraphs\Entity\BreezyParagraphsVariantInterface $variant */
    $variant = $this->entityTypeManager->getStorage('breezy_paragraphs_variant')->load($variant_id);
    return $variant;
  }

}
