<?php

namespace Drupal\breezy_paragraphs_ui\Form;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Provides a form to delete a BreezyParagraphsVariant entity.
 */
class BreezyParagraphsVariantDeleteForm extends EntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete %label', ['%label' => $this->entity->label()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.breezy_paragraphs_variant.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Load the entity to be deleted.
    $entity = $this->entity;

    // Delete the entity.
    $entity->delete();

    // Display a message indicating successful deletion.
    $this->messenger()->addStatus($this->t('The variant has been deleted.'));

    // Redirect the user to the collection page of BreezyParagraphsVariants.
    $form_state->setRedirect('entity.breezy_paragraphs_variant.collection');
  }

}
