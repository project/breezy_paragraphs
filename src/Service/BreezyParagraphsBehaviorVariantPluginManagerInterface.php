<?php

namespace Drupal\breezy_paragraphs\Service;

use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * Provides an interface for BreezyParagraphsBehaviorVariantPluginManager.
 */
interface BreezyParagraphsBehaviorVariantPluginManagerInterface extends PluginManagerInterface {

  /**
   * Validates plugin requirements.
   *
   * @param string $plugin_id
   *   The plugin id.
   *
   * @return bool
   *   True if the requirements are validated.
   */
  public function validateRequirements(string $plugin_id): bool;

  /**
   * Get the valid plugin definitions.
   *
   * @return array
   *   An array of valid plugin definitions keyed by plugin id.
   */
  public function getValidDefinitions(): array;

  /**
   * Get the paragraph type from a plugin id.
   *
   * @param string $plugin_id
   *   The plugin id.
   *
   * @return string
   *   The paragraph type from the plugin definition.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getParagraphType(string $plugin_id): string;

  /**
   * Get the paragraph elements from a plugin id.
   *
   * @param string $plugin_id
   *   The plugin id.
   *
   * @return array
   *   An array of elements from the plugin definition.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getParagraphElements(string $plugin_id): array;

}
