<?php

namespace Drupal\breezy_paragraphs\Storage;

use Drupal\Core\Config\Entity\ConfigEntityStorage;

/**
 * Defines the BreezyParagraphsVariant storage.
 */
class BreezyParagraphsVariantStorage extends ConfigEntityStorage implements BreezyParagraphsVariantStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function loadValid() {
    $query = $this->getQuery()->condition('status', 1);

    $result = $query->execute();

    if (empty($result)) {
      return [];
    }
    return $this->loadMultiple($result);
  }

}
