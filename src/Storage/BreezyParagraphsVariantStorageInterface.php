<?php

namespace Drupal\breezy_paragraphs\Storage;

use Drupal\Core\Config\Entity\ConfigEntityStorageInterface;

/**
 * Defines the interface for BreezyParagraphsVariant entities.
 */
interface BreezyParagraphsVariantStorageInterface extends ConfigEntityStorageInterface {

  /**
   * Loads the valid BreezyParagraphsVariant config entities.
   *
   * @return \Drupal\breezy_paragraphs\Entity\BreezyParagraphsVariantInterface[]
   *   An array of BreezyParagraphsVariantInterface entities.
   */
  public function loadValid();

}
