<?php

namespace Drupal\breezy_paragraphs_example_heading\Plugin\BreezyParagraphs\BehaviorVariant;

use Drupal\breezy_paragraphs\Attribute\BehaviorVariant;
use Drupal\breezy_paragraphs\Plugin\BreezyParagraphs\BehaviorVariant\BehaviorVariantBase;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\paragraphs\Entity\Paragraph;

/**
 * Provides an example heading paragraph variant plugin.
 */
#[BehaviorVariant(
  id: 'example_heading',
  label: new TranslatableMarkup("Example Heading"),
  description: new TranslatableMarkup("Example heading paragraph behavior variant."),
  paragraph_type: 'example_heading',
  paragraph_elements: [
    'heading_text' => new TranslatableMarkup("Heading text"),
  ],
)]
class ExampleHeading extends BehaviorVariantBase {

  /**
   * {@inheritdoc}
   */
  public function view(array &$build, Paragraph $paragraph, EntityViewDisplayInterface $display, string $view_mode, array $behavior_form_settings, array $variant_classes) {
    $content = [];
    foreach (Element::children($build) as $field) {
      $content[] = $build[$field];
    }

    $heading_text_classes = [];
    if (isset($variant_classes['heading_text'])) {
      $classes = $variant_classes['heading_text'];
      foreach ($classes as $class) {
        $heading_text_classes[] = $class;
      }
    }

    if (isset($build['#attributes']['class'])) {
      $build['#attributes']['class'] += $variant_classes['heading_text'];
    }
    else {
      $build['#attributes'] = [
        'class' => $heading_text_classes,
      ];
    }
  }

}
