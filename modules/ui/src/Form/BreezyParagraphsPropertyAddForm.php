<?php

namespace Drupal\breezy_paragraphs_ui\Form;

use Drupal\breezy_paragraphs\Entity\BreezyParagraphsVariant;
use Drupal\breezy_paragraphs\Entity\BreezyParagraphsVariantInterface;
use Drupal\breezy_utility\BreezyUtilityClassServiceInterface;
use Drupal\breezy_utility\BreezyUtilityElementPluginManagerInterface;
use Drupal\breezy_utility\Form\BreezyUtilityDialogFormTrait;
use Drupal\breezy_utility\Utility\BreezyUtilityDialogHelper;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an add property form.
 */
class BreezyParagraphsPropertyAddForm extends FormBase {

  use BreezyUtilityDialogFormTrait;

  /**
   * Drupal\breezy_utility\BreezyUtilityClassServiceInterface definition.
   *
   * @var \Drupal\breezy_utility\BreezyUtilityClassServiceInterface
   */
  protected BreezyUtilityClassServiceInterface $classService;

  /**
   * BreezyUtilityElementPluginManagerInterface definition.
   *
   * @var \Drupal\breezy_utility\BreezyUtilityElementPluginManagerInterface
   */
  protected BreezyUtilityElementPluginManagerInterface $elementManager;

  /**
   * Placeholder variant entity.
   *
   * @var \Drupal\breezy_paragraphs\Entity\BreezyParagraphsVariantInterface
   */
  protected BreezyParagraphsVariantInterface $variant;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    /** @var \Drupal\breezy_utility\BreezyUtilityClassServiceInterface $class_service */
    $class_service = $container->get('breezy_utility.utility_classes');
    $instance->classService = $class_service;
    /** @var \Drupal\breezy_utility\BreezyUtilityElementPluginManagerInterface $element_manager */
    $element_manager = $container->get('plugin.manager.breezy_utility.element');
    $instance->elementManager = $element_manager;
    $instance->variant = BreezyParagraphsVariant::create(['id' => '_variant_temp_form']);
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'breezy_paragraphs_ui_add_property_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, BreezyParagraphsVariantInterface $breezy_paragraphs_variant = NULL, string $type = NULL) {
    $parent_key = $this->getRequest()->query->get('parent');

    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';
    $form['#attached']['library'][] = 'breezy_utility/breezy_utility.ajax';

    $property_wrapper_id = 'property-wrapper';

    $form['#attributes'] = [
      'id' => $property_wrapper_id,
    ];

    $form['property_type'] = [
      '#type' => 'breezy_utility_property_autocomplete',
      '#title' => $this->t('Choose property'),
      '#description' => $this->t('Enter a CSS property name. Only one property may be added. Example: margin, padding, display.'),
      '#default_value' => $form_state->getValue('property_type') ?? '',
      '#required' => TRUE,
      '#ajax' => [
        'callback' => '::changePropertyType',
        'wrapper' => $property_wrapper_id,
        'event' => 'autocompleteclose',
        'disable-refocus' => TRUE,
      ],
    ];

    $property_type = $form_state->get('property_type');
    if ($form_state->getValue('property_type')) {
      $property_type = $form_state->getValue('property_type');
    }

    if ($property_type && !$this->isValidProperty($property_type)) {
      $property_type = FALSE;
      $form['property_type_message'] = [
        '#markup' => $this->t('No properties found.'),
      ];
    }

    if ($property_type) {
      $form['elements'] = [
        '#type' => 'table',
        '#caption' => $this->t('Choose element type'),
        '#header' => [$this->t('Type'), $this->t('Description'), ''],
      ];

      $elements = $this->getElementOptions();
      foreach ($elements as $element_type => $element_definition) {
        $row = [];
        $query = [
          'property' => $property_type,
        ];
        if ($parent_key) {
          $query['parent'] = $parent_key;
        }

        $url = Url::fromRoute('entity.breezy_paragraphs_ui.element.add_form', [
          'breezy_paragraphs_variant' => $breezy_paragraphs_variant->id(),
          'type' => $element_type,
        ],
          [
            'query' => $query,
          ]);
        $row['link'] = [
          '#type' => 'link',
          '#title' => $element_definition['label'],
          '#url' => $url,
          '#attributes' => BreezyUtilityDialogHelper::getOffCanvasDialogAttributes(),
        ];

        $row['description'] = [
          '#markup' => $element_definition['description'],
        ];

        $row['operation'] = [
          '#type' => 'link',
          '#title' => $this->t('Add element'),
          '#url' => $url,
          '#attributes' => BreezyUtilityDialogHelper::getOffCanvasDialogAttributes('normal', ['button']),
        ];
        $form['elements'][$element_type] = $row;
      }
    }

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    $property_type = $form_state->getValue('property_type');
    $properties = $this->classService->getPropertyOptions();
    if (!array_key_exists($property_type, $properties)) {
      $form_state->setErrorByName('property_type', 'Invalid property type.');
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->clearErrors();
    $form_state->setRebuild();
  }

  /**
   * Submit form #ajax callback.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   An Ajax response that display validation error messages or redirects
   *   to a URL.
   */
  public function submitAjaxForm(array &$form, FormStateInterface $form_state): AjaxResponse {
    // Remove #id from wrapper so that the form is still wrapped in a <div>
    // and triggerable.
    // @see js/webform.element.details.toggle.js
    $form['#prefix'] = '<div>';

    $response = new AjaxResponse();
    $response->addCommand(new HtmlCommand('#breezy-ui-element-ajax-wrapper', $form));
    return $response;
  }

  /**
   * Get element options.
   *
   * @return array
   *   An array of element plugins.
   */
  protected function getElementOptions(): array {
    $elements = [];
    $element_definitions = $this->elementManager->getValidDefinitions();
    foreach ($element_definitions as $id => $definition) {
      if ($definition['hidden'] === TRUE) {
        continue;
      }
      $elements[$id] = $definition;
    }
    return $elements;
  }

  /**
   * Callback when property type is changed.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return array
   *   The entire form array.
   */
  public function changePropertyType(array &$form, FormStateInterface $form_state): array {
    return $form;
  }

  /**
   * Checks if this a property is valid.
   *
   * @param string $property_name
   *   The property name.
   *
   * @return bool
   *   True is the property name exists.
   */
  protected function isValidProperty(string $property_name = ''): bool {
    if (empty($property_name)) {
      return FALSE;
    }
    $properties = $this->classService->getPropertyOptions();
    return array_key_exists($property_name, $properties);
  }

}
