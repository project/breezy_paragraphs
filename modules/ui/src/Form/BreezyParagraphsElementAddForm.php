<?php

namespace Drupal\breezy_paragraphs_ui\Form;

use Drupal\breezy_paragraphs\Entity\BreezyParagraphsVariantInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a form for adding elements.
 */
class BreezyParagraphsElementAddForm extends BreezyParagraphsElementFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'breezy_paragraphs_ui_element_add_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, BreezyParagraphsVariantInterface $breezy_paragraphs_variant = NULL, $key = NULL, $parent_key = NULL, $type = NULL) {
    $this->property = $this->getRequest()->query->get('property');
    $this->parentKey = $this->getRequest()->query->get('parent');

    $this->key = $key ?? '';
    $this->element['#type'] = $type;
    $this->variant = $breezy_paragraphs_variant;

    $element_plugin = $this->getElementPlugin();

    $form['#title'] = $this->t('Add @label', ['@label' => $element_plugin->label()]);
    $form_state->set('property', $this->property);

    $form = parent::buildForm($form, $form_state, $breezy_paragraphs_variant, $key, $this->parentKey, $type);

    return $form;
  }

}
